<?php

use App\Mechanic;
use Illuminate\Database\Migrations\Migration;

class OverwriteExistingMechanics extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Mechanic::all()->each(function ($mechanic) {
            $mechanic->delete();
        });

        $mechanics = [
            [
                'sequence_number' => 1,
                'content' => '<p>In the competition, participating secondary schools will work with their students, the Parent Teachers and Community Association (PTCA), and other community-based groups to collect <span class="font-weight-bold">used</span> PET bottles and showcase an existing or create a new innovative solution for <span class="font-weight-bold">upcycling<sup>2</sup> </span> the PET bottles not only to address the issue of plastic waste pollution but also reusing these PET bottles as a means to address other challenges or pursue opportunities in their community and add value in their day to day lives such as, but not limited to secure livelihoods, community infrastructure, health, disaster risk reduction, and other development projects. </p>',
            ],

            [
                'sequence_number' => 2,
                'content' => '<p>Participating secondary schools should partner with an active and registered community-based organization, cooperative or association within their respective communities.  Additional points will be given to upcycling solutions that involve partnerships with local businesses, the private sector, other NGOs, and their local government unit.</p>',
            ],

            [
                'sequence_number' => 3,
                'content' => '<p>Solution search participants must document the process and output of their innovation through an accomplished entry form provided by Nature Spring Foundation, Inc.</p>',
            ],

            [
                'sequence_number' => 4,
                'content' => '<p>The project solution must involve the participation of children of the school, the local youth, and women of the community. A project committee structure must actively involve these groups in decision-making.</p>',
            ],
            // [
            //     'sequence_number' => 5,
            //     'content' => '<p>Participants will be given a maximum of three months to identify existing solutions and enhance them based on the project solution search criteria OR to conceptualize, successfully test a working proto-type or model and document their innovation. Submission of entries is due on August 2019.</p>',
            // ],

            // [
            //     'sequence_number' => 6,
            //     'content' => '<p>The competition is limited to one innovation per participating entry.</p>',
            // ],
        ];

        foreach ($mechanics as $mechanic) {
            Mechanic::create($mechanic);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
