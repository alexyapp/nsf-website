<?php

use App\Guideline;
use Illuminate\Database\Migrations\Migration;

class UpdateGuidelines extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Guideline::all()->each(function ($guideline) {
            $guideline->delete();
        });

        $guidelines = [
            [
                'sequence_number' => 1,
                'content' => '<p>Nature’s Spring Foundation Inc. (NSFI) shall send an invitation letter, with an attached confirmation slip, to all public secondary schools through the Division Offices of the Department of Education. Information dissemination should be at least three months prior to the deadline for submission of entries.</p>',
            ],

            [
                'sequence_number' => 2,
                'content' => '<p>Secondary schools shall officially notify NSFI in writing of its interest to join the competition through signing and sending the confirmation slip to the mailing or email addresses provided. A team of secondary school and the community-based organization will be called a <span class="font-weight-bold">school-unity <sup>3</sup></span> entry to suggest inclusiveness of participants per entry. The Official Launch of the competition is set on the Birgada Caravan of DepEd.</p>',
            ],

            [
                'sequence_number' => 3,
                'content' => '<p>After the official launch of the competition, NSFI will invite the school-unity entries in the mentoring and equipping workshops to assist schools in developing and/or enhancing their project ideas, innovations or existing upcycling projects. The workshop series will be conducted per division to accommodate schools situated in far-flung and remote areas. Interested school-unity entries shall then undergo mentoring sessions with the pool of mentors in the workshop.</p>',
            ],

            [
                'sequence_number' => 4,
                'content' => '<p>All participating school-unity entries both mentored or not, shall be given three (3) months to identify existing solutions and enhance them based on the project solution search criteria OR to conceptualize, successfully test a working proto-type or model and document their innovation. The deadline of submission is due on September 30, 2019.</p>',
            ],

            [
                'sequence_number' => 5,
                'content' => '<p>Participants must submit the accomplished entry form provided by Nature’s Spring Foundation. Participants must also submit photos and/or videos about the process and the innovation itself. Requirements can be submitted online or through physical copies. Participants can send their entries to thebotechallenge@naturespringfoundation.org or through mailing the physical copy to the address:</p><ul><li><p>Nature’s Spring Foundation</p><p>Serviced Offices, 5th floor, Bai Hotel, CSSEAZ, Mandaue City</p></li></ul>',
            ],

            [
                'sequence_number' => 6,
                'content' => '<p>NSFI shall check the requirements submitted by all the school-unity entries. Failure to complete requirements is an automatic disqualification of a school-unity entry. Likewise, participating school-unity entries which were not able to meet the criteria of selection shall be automatically disqualified. Other possible grounds that may cause disqualification are:</p><ul><li>Plagiarized idea of innovation</li><li>Entries who violate the Child Labor Laws</li></ul>',
            ],

            [
                'sequence_number' => 7,
                'content' => '<p>NSFI organizers shall then forward all qualified school-unity entries to the NSFI Screening Committee for the first phase of judging. In this phase, the NSFI Screening Committee will select screen the top twenty (20) finalists of the competition. The criteria for the top 20 selection are as follows:</p><ul><li>Impact: 30%</li><li>Sustainability of innovation: 30%</li><li>Profitability/Market potential of the innovation: 30%</li><li><p>Developmental*: 10%</p><small>(Addresses additional challenges or provide additional opportunities in the field of gender and development, education, health, disaster risk reduction, climate change, etc.)</small></li></ul>',
            ],

            [
                'sequence_number' => 8,
                'content' => '<p>The second phase of the screening will focus on the selection of the Top Ten (10) finalists. In this phase, the NSFI Screening Committee will forward the entries to the Identified Panel of Judges. NSFI will open the Public Voting for the top 10 winners through the website. The finalists will be informed of the modified judging criteria for the 1st-10th prizes. The percentage of votes shall be taken from the following:</p><ul><li>Panel of Judges ------ 70%</li><li>Public Votes ---------- 30%</li></ul>',
            ],

            [
                'sequence_number' => 9,
                'content' => '<p>Once the public voting closes, NSFI shall immediately calculate the percentage scores from the public votes and the panel of judges. Winners will be announced during the Awarding Ceremony set on October or November 2019. The Top Twenty (20) finalists will be showcased during the ceremony. For transparency, results will be presented during the Awarding Ceremony.</p>',
            ],

            [
                'sequence_number' => 10,
                'content' => '<p>Winners will then accept their corresponding cash prizes which will be converted into a project of their choice. The prizes are as follows:</p><ul><li>Grand prize – 750,000.00</li><li>Second prize – 400,000.00</li><li>Third prize – 200,000.00</li><li>4th to 10th prizes –80,000.00 each</li></ul><p>The prize money shall be given in support of the cycling project such as, but not limited to:</p><ul><li>Raw materials inventory</li><li>Business infrastructure</li><li>Supplies and equipment</li><li>Training and technical assistance</li><li>Operating capital</li><li>Business development services</li></ul>',
            ],

            [
                'sequence_number' => 11,
                'content' => '<p>NSFI shall assist the winning school-unity entries during the whole process of receiving the prize—from the planning to the implementation of their respective projects. Since every entry is a partnership effort with the school and the community-based organization, NSFI must ensure that the project proposed by the winners is a joint venture that would equally benefit both parties.</p>',
            ],
        ];

        foreach ($guidelines as $guideline) {
            Guideline::create($guideline);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
