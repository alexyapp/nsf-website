<?php

use App\Faq;
use Illuminate\Database\Migrations\Migration;

class AddMoreFaqs extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $faqs = [
            [
                'sequence_number' => 16,
                'question' => 'Are integrated schools included?',
                'answer' => '<p>Yes, integrated schools can join the competition as long as the students included by the school in the competition are the high school students. It should be indicated in the entry form.</p>',
            ],
            [
                'sequence_number' => 17,
                'question' => 'Can Senior High School Students join?',
                'answer' => '<p>Yes, Senior High School students can definitely join. It will be the discretion of the school on which group of schools they want to lead for the competition.</p>',
            ],
            [
                'sequence_number' => 18,
                'question' => 'Can we partner with other organizations or institutions aside from the community-based organization?',
                'answer' => '<p>A school can definitely partner with other organizations or institutions (could be the LGU, government agency, other non-government or non-profit organizations, corporations and etc.), as many as the school can partner. However, the school should not forget to partner with one community-based organization as it is a part of the criteria. Remember that your entry will be called a school-unity, which means one entry is composed of a school and a community-based org. </p>',
            ],
            [
                'sequence_number' => 19,
                'question' => 'Can two or more schools tie up with the same community-based organization?',
                'answer' => '<p>No. Any two or more entries submitted with the same partner-organizations will automaticall by disqualified in the competition during the pre-screening process.</p>',
            ],
            [
                'sequence_number' => 20,
                'question' => 'What if we have the same innovation with the other schools?',
                'answer' => '<p>It is the task of the school-unity to make sure that the product/service innovation they have developed has its own unique value proposition, meaning there are no other innovation the same with it. There might be similarities with other school-unities but it is your innovation’s unique value proposition that could attract the judges in the competition.</p>',
            ],
            [
                'sequence_number' => 21,
                'question' => 'What if there are no registered community-based organizations in our area?',
                'answer' => '<p>The community-based organization don’t necessarily need a registration as long as the entry can present a document that the organization is an active, running organization recognized by the Barangay or the LGU.</p>',
            ],
            [
                'sequence_number' => 22,
                'question' => 'Won’t this project encourage students to buy more plastics instead of reducing it?',
                'answer' => '<p>Since the challenge seeks for solutions to reduce the plastic bottles and not buy more, we do not encourage schools to buy more plastic bottles just for the sake of the competition. We do not limit the number of bottles that every school should prepare, nor the brand that they need for their innovation. As long as the school can present a way of collection and segregation as an anchored practice/system together with the innovation. The goal is to close the loop and continue a circular system for the plastic economy, and eventually avoid it from ending in sewage systems, polluting rivers, seas and ocean.</p>',
            ],
        ];

        foreach ($faqs as $faq) {
            Faq::create($faq);
        }

        Faq::find(15)->update([
            'answer' => '<p>There will be a total of 10 declared winners. The ten winners will then accept their corresponding cash prizes which will be converted into a project of their choice. The prizes are as follows:</p><ul style="list-style-type: lower-roman"><li>Grand prize  750,000.00</li><li>Second prize – 400,000.00</li><li>Third prize – 200,000.00</li><li>4th to 10th prizes – 80,000.00</li></ul><p>The prize money shall be given in support of the cycling project such as, but not limited to:</p><ul><li>Raw materials inventory</li><li>Business infrastructure</li><li>Supplies and equipment</li><li>Training and technical assistance</li><li>Operating capital</li><li>Business development services</li></ul><p>Winners of the solution search shall officially become grant recipients of NSFI and shall be covered by terms and conditions as provided by a Grant Agreement to be signed by all parties.</p><p>For more details on the prizes, please see the <a href="/how-to-join/guidelines">guidelines for prizes</a></p>',
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
    }
}
