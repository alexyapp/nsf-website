<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Mechanic;
use Faker\Generator as Faker;

$factory->define(Mechanic::class, function (Faker $faker) {
    return [
        'sequence_number' => $faker->randomDigit,
        'content' => $faker->paragraph
    ];
});
