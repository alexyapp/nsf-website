<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Guideline;
use Faker\Generator as Faker;

$factory->define(Guideline::class, function (Faker $faker) {
    return [
        'sequence_number' => $faker->randomDigit,
        'content' => $faker->paragraph
    ];
});
