<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Faq;
use Faker\Generator as Faker;

$factory->define(Faq::class, function (Faker $faker) {
    return [
        'sequence_number' => $faker->randomDigit,
        'question' => $faker->words,
        'answer' => $faker->sentence
    ];
});
