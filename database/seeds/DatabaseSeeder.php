<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GuidelinesTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(MechanicsTableSeeder::class);
        $this->call(CriteriasTableSeeder::class);
    }
}
