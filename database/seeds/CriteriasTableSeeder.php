<?php

use App\Criteria;
use Illuminate\Database\Seeder;

class CriteriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $criterias = [
            [
                'sequence_number' => 1,
                'content' => '<p>Public Secondary School must have an active partnership with a community-based organization, cooperative or association.</p>',
            ],

            [
                'sequence_number' => 2,
                'content' => '<p>Active participation of children of the school, the local youth, and women of the community. A project committee structure must actively involve these groups in decision-making.</p>',
            ],

            [
                'sequence_number' => 3,
                'content' => '<p>The criteria for the selection are as follows:</p><ul><li>Impact: 30%</li><li>Sustainability of innovation: 30%</li><li>Profitability/Market potential of the innovation: 30%</li><li>Developmental*: 10%</li></ul><p>(Addresses additional challenges or provide additional opportunities in the field of gender and development, education, health, disaster risk reduction, climate change, etc.)</p>',
            ],
        ];

        factory(Criteria::class, sizeof($criterias))->make()->each(function ($criteria, $i) use ($criterias) {
            $criteria->sequence_number = $criterias[$i]['sequence_number'];
            $criteria->content = $criterias[$i]['content'];
            $criteria->save();
        });
    }
}
