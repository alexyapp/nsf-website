<?php

use App\Faq;
use Illuminate\Database\Seeder;

class FaqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faqs = [
            [
                'sequence_number' => 1,
                'question' => 'What is the BOTE Challenge?',
                'answer' => '<p>The BOTE Challenge is an annual search for sustainable and innovative solutions on plastic waste management practices which actively involves the children and youth, community-based sectors, the local government unit and other sectors within communities.</p>',
            ],

            [
                'sequence_number' => 2,
                'question' => 'Who are qualified to join the BOTE Challenge?',
                'answer' => '<p>The competition is open to all public secondary schools in the province of Cebu, excluding the schools situated in the Metro Cebu Area—Cebu City, Mandaue City, Lapu-lapu City, and Talisay City.</p><p>But this comes with a twist. The public secondary schools should partner with an active community-based organization, association or cooperative in the process of conceptualizing and implementing their innovation for them to qualify the BOTE Challenge.</p>',
            ],

            [
                'sequence_number' => 3,
                'question' => 'Why is the Metro Cebu Area not included in the coverage?',
                'answer' => '<p>These areas are excluded to prevent the disruption of livelihood of peddlers and scavengers involved in the collection and recycling of plastic bottles.</p>',
            ],
            

            [
                'sequence_number' => 4,
                'question' => 'How do I join the BOTE Challenge?',
                'answer' => '<p>To join the BOTE Challenge, the school must simply team up with an active community-based organization where members of the community work together in creating and implementing their innovative and sustainable solution on plastic waste upcycling and management. A team of students, teachers, parents and members of the community-based organization will now be called as a School-unity.</p>',
            ],

            [
                'sequence_number' => 5,
                'question' => 'When is the deadline of the BOTE Challenge?',
                'answer' => '<p>All interested school-unities will be given three (3) months to identify existing solutions and enhance them based on the project solution search criteria or to concept.tualize, successfully test a working proto-type or model and document their innovation. Submission of entries is due on August 31, 2019.</p>',
            ],

            [
                'sequence_number' => 6,
                'question' => 'Where and how will we submit our entry?',
                'answer' => '<p>To submit your entries, please download and fill-up the entry form, prepare the attachments needed, submit to application@naturespringfoundation.org and wait for NSFI staff to confirm the receipt of your email. You may also submit physical files of your entries to the NSFI Office located at the Serviced Offices, 5th floor, Bai Hotel, CSSEAZ, CSSEAZ, Mandaue City.</p>',
            ],

            [
                'sequence_number' => 7,
                'question' => 'Can I submit more than one entry?',
                'answer' => '<p>No, the competition is limited to one innovation per participating entry.</p>',
            ],

            [
                'sequence_number' => 8,
                'question' => 'How did the BOTE Challenge started?',
                'answer' => '<p>The Bote Challenge started when Nature’s Spring Foundation sought the need to develop lasting solutions for responsible waste management and promote a healthier, more liveable, and sustainable future for our people and our world. Such solutions will be recognized as bright spots and help build a movement of initiatives that would counter the projected effects of improper waste disposal on water, health, livelihoods, climate change and disaster risks.</p>',
            ],

            [
                'sequence_number' => 9,
                'question' => 'What is upcycling?',
                'answer' => '<p>Upcycling, also known as creative reuse, is the process of transforming by-products, waste materials, useless, or unwanted products into new materials or products of better quality or for better environmental value. Upcycling is the opposite of downcycling, which is the other face of the recycling process.</p>',
            ],

            [
                'sequence_number' => 10,
                'question' => 'What is School-unity?',
                'answer' => '<p>Every school-unity entry must consist of the secondary student leaders, teachers and parents from the PTCA Association and officers from the community-based organization, cooperative or association within their respective communities. The community-based organization must be based in the barangay where the secondary school is situated or in any adjacent barangays.</p>',
            ],

            [
                'sequence_number' => 11,
                'question' => 'What is the purpose of the mentoring workshop?',
                'answer' => '<p>The mentoring workshop is a 1-day activity designed to assist all participating school-unity entries to develop and/or enhance their project ideas, innovations or existing upcycling project, and ensure that the projects they develop are geared towards sustainable development. The mentoring workshops are free. Nature’s Spring Foundation will be disseminating a list of schedules for the free mentoring workshops in their respective areas where they can attend for free.</p>',
            ],

            [
                'sequence_number' => 12,
                'question' => 'How do we get to avail the mentoring workshop?',
                'answer' => '<p>To avail the mentoring workshop, the school must check the schedule of the mentoring workshops with their respective divisions. The school must confirm their attendance to the event. Each school-unity entry can bring a maximum of 5 participants during the workshop.</p>',
            ],

            [
                'sequence_number' => 13,
                'question' => 'What if we have the same innovation with the other schools?',
                'answer' => '<p>Entries with almost the same innovation will still be accepted in the challenge. They key questions that this challenge impose is, how sustainable is the innovation? Innovations might look the same but every school-unity entry is unique. All entries with the same innovation will still go through a thorough review during the screening process. Whoever meets the most of the criteria during the selection will excel in the challenge. </p>',
            ],

            [
                'sequence_number' => 14,
                'question' => 'When and how will we know the results or winners of the competition?',
                'answer' => '<p>NSFI organizers shall then forward all qualified school-unity entries to the NSFI Screening Committee for the first phase of judging. In this phase, the NSFI Screening Committee will select screen the top twenty (20) finalists of the competition. After the first phase of the screening, NSFI will inform all the school-unity entries if they have successfully made it to the top twenty.</p><p>The second phase of the screening will focus on the selection of the Top Ten (10) finalists. In this phase, the NSFI Screening Committee will forward the entries to the Identified Panel of Judges. NSFI will open the Public Voting for the top 10 winners through the website. The finalists will be informed of the modified judging criteria for the 1st-10th prizes. The percentage of votes shall be taken from the following:</p><ul><li>Panel of Judges ------  70%</li><li>Public Votes ----------  30%</li></ul><p>Once the public voting closes, NSFI shall immediately calculate the percentage scores from the public votes and the panel of judges. Winners will be announced during the Awarding Ceremony set on October or November 2019.</p>',
            ],

            [
                'sequence_number' => 15,
                'question' => 'How many winners will be declared? What is the prize for each winner?',
                'answer' => '<p>There will be a total of 10 declared winners. The ten winners will then accept their corresponding cash prizes which will be converted into a project of their choice. The prizes are as follows:</p><ul style="list-style-type: lower-roman"><li>Grand prize  750,000.00</li><li>Second prize – 400,000.00</li><li>Third prize – 200,000.00</li><li>4th to 10th prizes – 80,000.00</li></ul><p>The prize money shall be given in support of the cycling project such as, but not limited to:</p><ul><li>Raw materials inventory</li><li>Business infrastructure</li><li>Supplies and equipment</li><li>Training and technical assistance</li><li>Operating capital</li><li>Business development services</li></ul><p>Winners of the solution search shall officially become grant recipients of NSFI and shall be covered by terms and conditions as provided by a Grant Agreement to be signed by all parties.</p>',
            ],
        ];

        factory(Faq::class, sizeof($faqs))->make()->each(function ($faq, $i) use ($faqs) {
            $faq->sequence_number = $faqs[$i]['sequence_number'];
            $faq->question = $faqs[$i]['question'];
            $faq->answer = $faqs[$i]['answer'];
            $faq->save();
        });
    }
}
