@extends('layouts.app')

@push('styles')
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css">
	<style>
		body {
			padding-bottom: 0 !important;
		}

		#app {
			background-color: #066AB4;
		}

		.finalist-details {
			top: 0;
			z-index: 1001;
		}

		.overlay {
			background-color: #000;
			z-index: 1000;
			top: 0;
		}

		.finalist-details, .overlay {
			opacity: 0;
			transition: all .3s ease;
		}

		.slick-dots li.slick-active button:before, .slick-dots li button:before {
			color: #fff;
		}

		.finalist-cover-photo-container {
			overflow-x: hidden;
			height: 200px;
		}

		.finalist-cover-photo {
			width: auto;
			height: 100%;
			position: relative;
			transform: translateX(-50%);
			left: 50%;
		}
	</style>
@endpush

@push('scripts')
	{{-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script> --}}
	<script>
		$(document).ready(function() {
			$('.finalist').hover(function() {
				var $this = $(this);

				$this.find('.finalist-details').css({
					opacity: 1
				});

				$this.find('.overlay').css({
					opacity: .3
				});
			}, function() {
				var $this = $(this);

				$this.find('.finalist-details').css({
					opacity: 0
				});

				$this.find('.overlay').css({
					opacity: 0
				});
			});
		});
	</script>
@endpush

@section('content')
    @include('partials.header', ['title' => 'Vote for your Favorite Finalist', 'subtitle' => '30% of the results will come from the votes. The voting period will run from October 24 to November 2, 2019.'])

    <div class="container">
		<div class="row">
			<div class="col-12 text-center">
				<h2 style="font-size: 4rem; padding: 40px 0;" class="nature-spirit-regular text-white mb-0;">MEET THE TOP 15 FINALISTS</h2>
			</div>
		</div>
		<div class="row" style="padding-bottom: 40px;">
			<div class="col-12">
				@if ($finalists->count())
					<div class="slider">
						@foreach ($finalists as $i => $finalist)
							@if (!($i + 1 > ceil($finalists->count() / 2)))
								<div class="finalist-wrapper p-2">
									@foreach ($finalists_even as $index => $finalist)
										@if ($i == $index)
											<div class="finalist position-relative m-2 even">
												<div class="finalist-cover-photo-container">
													<img class="finalist-cover-photo" src="{{ asset("thumbnails/{$finalist->profile_picture}") }}" alt="Entry Cover Photo">
												</div>
												<div class="finalist-details position-absolute w-100 text-center p-2 h-100 text-white">
													<p class="mb-0">{{ $finalist->name }}</p>
													<p>{{ str_limit($finalist->description, 20) }}</p>
													<a href="{{ route('contestants.show', $finalist->id) }}" class="btn btn-primary">Vote</a>
												</div>
												<div class="overlay w-100 h-100 position-absolute"></div>
											</div>
										@else
											@continue
										@endif
									@endforeach
	
									@foreach ($finalists_odd as $index => $finalist)
										@if ($i == $index)
											<div class="finalist position-relative m-2 odd">
												<div class="finalist-cover-photo-container">
													<img class="finalist-cover-photo" src="{{ asset("thumbnails/{$finalist->profile_picture}") }}" alt="Entry Cover Photo">
												</div>
												<div class="finalist-details position-absolute w-100 text-center p-2 h-100 text-white">
													<p class="mb-0">{{ $finalist->name }}</p>
													<p>{{ str_limit($finalist->description, 20) }}</p>
													<a href="{{ route('contestants.show', $finalist->id) }}" class="btn btn-primary">Vote</a>
												</div>
												<div class="overlay w-100 h-100 position-absolute"></div>
											</div>
										@else
											@continue
										@endif
									@endforeach
								</div>
							@endif
						@endforeach
					</div>
				@else
					<div>
						<h3>No finalists...</h3>
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection