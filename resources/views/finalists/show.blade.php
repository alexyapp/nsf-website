@extends('layouts.app')

@push('styles')
    <style>
        body {
            padding-bottom: 0 !important;
        }

        #app {
			background-color: #066AB4;
        }
        
        .finalist-details h2 {
            color: #066AB4;
        }
    </style>
@endpush

@section('content')
    @include('partials.header', ['title' => 'VOTING PAGE'])

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-9">
                    @if (session('has_voted'))
                        <div class="alert alert-danger mt-3 mb-0">
                            {{ session('has_voted') }}
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success mt-3 mb-0">
                            {{ session('success') }}
                        </div>
                    @endif
            </div>
        </div>

        <div class="row">
			<div class="col-12 text-center">
				<h2 style="font-size: 4rem; padding: 40px 0;" class="nature-spirit-regular text-white mb-0;">MEET THE TOP 15 FINALISTS</h2>
			</div>
		</div>
        <div class="row justify-content-center" style="padding-bottom: 40px;">
            <div class="col-md-9">
                <div class="finalist-cover-photo">
                    <img class="img-fluid w-100" src="{{ asset("uploads/{$finalist->profile_picture}") }}" alt="Finalist Cover Photo">
                </div>
                
                <div class="container-fluid bg-white p-5">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="finalist-details">
                                <h2 style="font-size: 4rem;" class="nature-spirit-regular mb-0;">{{ $finalist->name }}</h2>
                                <p class="quicksand-bold mb-0">{{ $finalist->description }}</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="custom-control custom-checkbox mb-5">
                                <input type="checkbox" class="custom-control-input" id="consent">
                                <label class="custom-control-label quicksand-bold" for="consent">By voting, I agree to the <a href="{{ route('privacy-policy') }}" target="blank">privacy policy</a>, <a href="{{ route('terms-and-conditions') }}" target="blank">terms and conditions</a> of this competition</label>
                            </div>

                            @if (isset($voting_system_setting) && $voting_system_setting->value == 'on')
                                <a href="{{ route('finalists.vote', ['finalist_id' => $finalist->id]) }}" class="btn btn-primary w-100 shadow quicksand-bold finalist-vote-button" style="font-size: 2rem;">
                                    Vote
                                    {{-- <form action="{{ route('finalists.vote', ['finalist_id' => $finalist->id]) }}" class="d-none finalist-vote-form" method="POST">
                                        @csrf
                                    </form> --}}
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection