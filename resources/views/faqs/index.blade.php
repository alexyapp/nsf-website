@extends('layouts.app')

@push('styles')
    <style>
        .container li {
            font-size: 1.3rem;
        }

        .container p, .container li {
            font-family: "Quicksand Bold"
        }
    </style>
@endpush

@section('content')
    @include('partials.header', ['title' => 'FAQS'])
    
    @include('partials.subheader', ['subtitle' => 'FREQUENTLY ASKED QUESTIONS'])

    <div class="container">
        <div class="row">
            <div class="col-12">
                @foreach ($faqs as $faq)
                    <div id="{{ $faq->id }}" class="mb-5">
                        <p>{{ $faq->sequence_number }}. <span class="primary-color">{{ $faq->question }}</span></p>

                        <div>
                            {!! $faq->answer !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection