<nav class="navbar navbar-expand-md navbar-light">
    <div class="container-fluid position-relative px-5">
        <a style="top: -.5rem;" class="navbar-brand position-absolute p-0" href="{{ url('/') }}">
            <img src="{{ asset('images/nsf-logo.jpg') }}" alt="Nature's Spring Foundation Logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a href="{{ route('about-us') }}" class="nav-link din-condensed-bold text-white" style="font-size: 1.625rem;">ABOUT US</a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('mission-vision') }}" class="nav-link din-condensed-bold text-white" style="font-size: 1.625rem;">MISSION & VISION</a>
                </li>

                <li class="nav-item">
                    <a href="{{ route('our-programs') }}" class="nav-link din-condensed-bold text-white" style="font-size: 1.625rem;">OUR PROGRAMS</a>
                </li>

                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link din-condensed-bold text-white dropdown-toggle" style="font-size: 1.625rem;" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        THE BOTE CHALLENGE <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a href="{{ route('about-the-bote-challenge') }}" class="dropdown-item">
                            ABOUT THE BOTE CHALLENGE
                        </a>

                        <a href="{{ route('finalists.index') }}" class="dropdown-item">
                            FINALISTS
                        </a>

                        <div class="dropdown">
                            <a href="#" class="dropdown-item dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">HOW TO JOIN</a>
                            <ul class="dropdown-menu" style="top: 0; left: 100%;">
                                <li>
                                    <a class="dropdown-item" href="{{ route('mechanics') }}">MECHANICS</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('guidelines') }}">GUIDELINES</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('criterias') }}">CRITERIA</a>
                                </li>
                            </ul>
                        </div>

                        <a class="dropdown-item" href="">
                            NEWS AND UPDATES
                        </a>

                        <a class="dropdown-item" href="{{ route('faqs') }}">
                            FAQs
                        </a>
                    </div>
                </li>

                <li class="nav-item">
                    <a href="{{ route('calendar-of-events') }}" class="nav-link din-condensed-bold text-white" style="font-size: 1.625rem;">CALENDAR OF EVENTS</a>
                </li>

                {{-- <li class="nav-item">
                    <a href="" class="nav-link din-condensed-bold text-white" style="font-size: 1.625rem;">CONTACT US</a>
                </li> --}}

                <!-- Authentication Links -->
                @guest
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif --}}
                @else
                    {{-- <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li> --}}
                @endguest
            </ul>
        </div>
    </div>
</nav>