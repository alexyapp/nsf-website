<div class="header position-relative">
    <div style="width: 75%;" class="d-flex align-items-center h-100 mx-auto">
        <div>
            <h1 style="font-size: 6rem; z-index: 1002;" class="nature-spirit-regular text-white position-relative">{{ $title }}</h1>

            @if (isset($subtitle))
                <p style="position: relative; z-index: 1002;" class="text-white">30% of the results will come from the votes. The voting period will run from October 24 to November 2, 2019.</p>
            @endif
        </div>
    </div>

    @include('partials.search-form')
</div>