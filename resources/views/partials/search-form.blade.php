<div class="position-absolute search-form">
    <form action="{{ route('search') }}" method="GET">
        <div class="d-flex align-items-center">
            <div>
                <input type="text" placeholder="search" name="q" class="px-4 py-1 text-center border-0">
            </div>
            <button type="submit">
                <i class="fas fa-search"></i>
            </button>
        </div>
    </form>
</div>