<div style="height: 240px;" class="subheader position-relative px-5">
    <h1 style="opacity: .09;" class="nature-spirit-regular font-size-xl primary-color position-absolute">{{ $subtitle }}</h1>
    <h2 style="font-size: 10rem; top: 50%; transform: translateY(-50%);" class="nature-spirit-regular font-size-lg primary-color position-absolute">{{ $subtitle }}</h2>
</div>