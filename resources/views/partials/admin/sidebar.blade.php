<div id="wrapper">

    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
        <h5>
            <a class="text-white" href="/">naturespringfoundation.org</a>
        </h5>
      </div>
      <ul class="sidebar-nav">
        <li class="{{ request()->is('admin/dashboard') ? 'active' : '' }}">
          <a href="{{ route('admin.dashboard') }}"><i class="fa fa-home"></i>Home</a>
        </li>
        <li class="{{ request()->is('admin/contestants*') ? 'active' : '' }}">
          <a href="{{ route('contestants.index') }}"><i class="fa fa-users"></i>Contestants</a>
        </li>
        <li class="{{ request()->is('admin/settings*') ? 'active' : '' }}">
          <a href="{{ route('settings.index') }}"><i class="fa fa-cog"></i>General Settings</a>
        </li>
        <li class="{{ request()->is('admin/account*') ? 'active' : '' }}">
          <a href="{{ route('account.showChangePasswordForm') }}"><i class="fa fa-user-lock"></i>Security Settings</a>
        </li>
        <li>
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
              <i class="fa fa-sign-out-alt"></i>Sign Out
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
              </form>
          </a>
        </li>
      </ul>
    </aside>
  
    <div id="navbar-wrapper">
      <nav class="navbar navbar-inverse">
        <div class="container-fluid">
          <div class="navbar-header">
            <a href="#" class="navbar-brand" id="sidebar-toggle"><i class="fa fa-bars"></i></a>
          </div>
        </div>
      </nav>
    </div>
  
    <section id="content-wrapper">
        <div class="row">
          <div class="col-lg-12">
            @yield('content')
          </div>
        </div>
    </section>
  
  </div>