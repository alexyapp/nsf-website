<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        @foreach ($list_items as $list_item)
            <li class="breadcrumb-item {{ isset($list_item['is_current']) && $list_item['is_current'] ? 'active' : '' }}">
                @if (isset($list_item['link']))
                    <a href="{{ $list_item['link'] }}">{{ $list_item['label'] }}</a>
                @else
                    {{ $list_item['label'] }}
                @endif
            </li>
        @endforeach
    </ol>
</nav>