@extends('layouts.app')

@push('styles')
    <style>
        .container li {
            font-size: 1.1rem;
        }
    </style>
@endpush

@section('content')
    @include('partials.header', ['title' => 'HOW TO JOIN'])
    
    @include('partials.subheader', ['subtitle' => 'CONTEST MECHANICS'])

    <div class="container">
        <div class="row mb-5">
            <div class="col-12">
                <p> The competition is open to all public secondary schools in the province of Cebu, excluding the schools situated in the Metro Cebu Area—Cebu City, Mandaue City, Lapu-lapu City, and Talisay City. <sup>1</sup></p>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                @foreach ($mechanics as $mechanic)
                    <div id="{{ $mechanic->id }}" class="d-flex align-items-center mb-5">
                        <span class="font-size-md primary-color mr-5">{{ $mechanic->sequence_number }}.</span>
                        <div class="d-flex flex-column quicksand-bold">
                            {!! $mechanic->content !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="mb-5">
            <hr>
        </div>

        <div class="row">
            <div class="col-12">
                <p class="quicksand-bold">
                    <sup>1</sup> These areas are excluded to prevent the disruption of livelihood of peddlers and scavengers involved in the collection and recycling of plastic bottles.
                </p>
                <p class="quicksand-bold">
                    <sup>2</sup> Upcycling, also known as creative reuse, is the process of transforming by-products, waste materials, useless, or unwanted products into new materials or products of better quality or for better environmental value. Upcycling is the opposite of downcycling, which is the other face of the recycling process.
                </p>
            </div>
        </div>
    </div>
@endsection