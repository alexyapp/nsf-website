@extends('layouts.app')

@push('styles')
    <style>
        .container li {
            font-size: 1.1rem;
        }
    </style>
@endpush

@section('content')
    @include('partials.header', ['title' => 'HOW TO JOIN'])
    
    @include('partials.subheader', ['subtitle' => 'CRITERIA'])

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="d-flex align-items-center">
                    <span class="font-size-md primary-color mr-5 invisible">0.</span>
                    <p class="quicksand-bold">CRITERIA FOR SELECTION</p>
                </div>
                
                @foreach ($criterias as $criteria)
                    <div id="{{ $criteria->id }}" class="d-flex align-items-center mb-5">
                        <span class="font-size-md primary-color mr-5">{{ $criteria->sequence_number }}.</span>
                        <div class="d-flex flex-column quicksand-bold">
                            {!! $criteria->content !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection