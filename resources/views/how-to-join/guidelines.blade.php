@extends('layouts.app')

@push('styles')
    <style>
        .container li {
            font-size: 1.1rem;
        }
    </style>
@endpush

@section('content')
    @include('partials.header', ['title' => 'HOW TO JOIN'])
    
    @include('partials.subheader', ['subtitle' => 'GUIDELINES'])

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="mb-3 nature-spirit-regular primary-color">GUIDELINES: PROCESS OF THE COMPETITION</h1>

                @foreach ($guidelines as $guideline)
                    <div id="{{ $guideline->id }}" class="d-flex align-items-center mb-5">
                        <span class="font-size-md primary-color mr-5">{{ $guideline->sequence_number }}.</span>
                        <div class="d-flex flex-column quicksand-bold">
                            {!! $guideline->content !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12">
                <p class="quicksand-bold">
                    <span class="font-weight-bold"><sup>3</sup> School-unity</span> – every school-unity entry must consist of the secondary student leaders, teachers and parents from the PTCA Association and officers from the community-based organization, cooperative or association within their respective communities. The community-based organization must be based in the barangay where the secondary school is situated or in any adjacent barangays.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h1 class="mb-3 nature-spirit-regular primary-color">GUIDELINES: PRIZES FOR THE BOTE WINNERS</h1>
                <div class="d-flex align-items-center mb-5">
                    <span class="font-size-md primary-color mr-5">1</span>
                    <div class="d-flex flex-column quick-sand-bold">
                        <p class="quicksand-bold">The top 10 winners of the Bote Challenge will receive and split their grant awards between the community based organization’s upcycling project and the host secondary school:</p>
                        <ul class="quicksand-bold">
                            <li>
                                CBO Upcycling Project – 50% of total prize money, which will be used to further support and implement the upcycling solution;
                            </li>
                            <li>
                                Host Secondary school – 50% of the total prize money; which will be used to support a school improvement project jointly implemented by the PTCA.
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="d-flex align-items-center mb-5">
                    <span class="font-size-md primary-color mr-5">2</span>
                    <div class="d-flex flex-column quick-sand-bold">
                        <p class="quicksand-bold">Under the Republic Act 8525 or the Adopt-a-School Program Act of 1998, the examples of eligible school improvement projects that the secondary school and the PTCA can propose are:</p>
                        <ul class="quicksand-bold">
                            <li>
                                School Infrastructure, such as waste segregation, disposal, and composting facilities
                            </li>
                            <li>
                                Technology and Multi-media Support
                            </li>
                            <li>
                                Furniture and Appliances
                            </li>
                            <li>
                                Student Wellness, Health and Nutrition
                            </li>
                            <li>
                                Teaching Learning Aids and Devices
                            </li>
                            <li>
                                Teacher Training and Skills Development
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="d-flex align-items-center mb-5">
                    <span class="font-size-md primary-color mr-5">3</span>
                    <div class="d-flex flex-column quick-sand-bold">
                        <p class="quicksand-bold">Winners of the solution search shall officially become grant recipients of NSFI and shall be covered by terms and conditions as provided by a Grant Agreement to be signed by all parties. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection