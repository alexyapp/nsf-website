@extends('layouts.app')

@section('content')
    @include('partials.header', ['title' => 'TERMS AND CONDITIONS'])
    
    @include('partials.subheader', ['subtitle' => 'TERMS AND CONDITIONS'])

    <div class="container">
        <p class="quicksand-bold">Last updated: August 19, 2019</p>
        <p class="quicksand-bold">Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the www.naturespringfoundation.org website (the "Service") operated by Nature's Spring Foundation ("us", "we", or "our")</p>
        
        <p class="quicksand-bold">Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>

        <p class="quicksand-bold">By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service. The Terms and Conditions agreement for Nature's Spring Foundation has been created with the help of TermsFeed.</p>

        <h4 class="nature-spirit-regular primary-color">Accounts</h4>

        <p class="quicksand-bold">When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of  the Terms, which may result in immediate termination of your account on our Service.</p>

        <p class="quicksand-bold">You are responsible for safeguarding the password that you use to access the Service and for any activities under your password, whether your password is with our Service or a third-party service.</p>

        <p class="quicksand-bold">You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.</p>

        <h4 class="nature-spirit-regular primary-color">Links To Other Web Sites</h4>

        <p class="quicksand-bold">Our Service may contain links to third-party web sites or services that are not owned or controlled by Nature's Spring Foundation.</p>

        <p class="quicksand-bold">Nature's Spring Foundation has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that Nature's Spring Foundation shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>

        <p class="quicksand-bold">We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.</p>

        <h4 class="nature-spirit-regular primary-color">Termination</h4>
        <p class="quicksand-bold">We may terminate or suspend access to our Service immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>

        <p class="quicksand-bold">All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>

        <p class="quicksand-bold">We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>

        <p class="quicksand-bold">Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.</p>

        <p class="quicksand-bold">All provisions of the Terms which by their nature should survive termination shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>

        <h4 class="nature-spirit-regular primary-color">Governing Law</h4>
        <p class="quicksand-bold">These Terms shall be governed and construed in accordance with the laws of Philippines, without regard to its conflict of law provisions.</p>

        <p class="quicksand-bold">Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>

        <h4 class="nature-spirit-regular primary-color">Changes</h4>
        <p class="quicksand-bold">We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>

        <p class="quicksand-bold">By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.</p>

        <h4 class="nature-spirit-regular primary-color">Contact Us</h4>
        <p class="quicksand-bold">If you have any questions about these Terms, please contact us.</p>
        <ul class="quicksand-bold">
            <li>By email: thebotechallenge@naturespringfoundation.org</li>
            <li>By phone number: (032) 355-8888 loc. 8794/8795/8798</li>
            <li>By mail: 5th floor, Serviced Offices, bai Hotel, CSSEAZ, Mandaue City, Cebu, Philippines, 6014</li>
        </ul>
    </div>
@endsection