@extends('layouts.app')

@section('content')
    @include('partials.header', ['title' => 'ABOUT THE BOTE CHALLENGE'])

    <div class="container-fluid overview py-5 d-flex align-items-center py-5">
        <div class="row">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <div style="height: 240px;" class="position-relative">
                    <h3 style="opacity: .09;" class="nature-spirit-regular font-size-xl primary-color position-absolute">OVERVIEW</h3>
                    <h3 style="font-size: 10rem; top: 50%; transform: translateY(-50%);" class="nature-spirit-regular font-size-lg primary-color position-absolute">OVERVIEW</h3>
                </div>
                <p class="quicksand-bold">The Bote Challenge seeks to recognize, promote, and adopt sustainable practices on proper plastic 
                        waste management through an annual search for a child-led, innovative solutions within communities 
                        that actively involves children and youth, women, community-based organizations, local governments, 
                        and other sectors in communities.
                        </p>
                <p class="quicksand-bold">Nature Spring Foundation, Inc. (NSFI) – the corporate social responsibility arm of the Philippine Spring 
                        Water Resources, Inc. (PSWRI), the makers of the Nature Spring bottled water - in partnership with the 
                        Department of Education (DepEd), local governments, the private sector, and other key stakeholders in 
                        Cebu, leads a nation-wide campaign to launch a solutions search competition with plastic waste
                        reduction as its central theme.  
                        </p>
                <p class="quicksand-bold">Starting its first year with Cebu province as a pilot, the competition is a strategy geared towards finding 
                        new or existing workable sustainable solutions that exist within our communities and has the potential
                        for adoption and replication in other communities.  Such solutions will be recognized as bright spots and
                        help build a movement of initiatives that would counter the projected effects of improper waste disposal 
                        on water, health, livelihoods, climate change and disaster risks. 
                        </p>
            </div>
        </div>
    </div>

    <div class="container-fluid p-5">
        <div class="row justify-content-center">
            <div class="col-12 d-flex flex-column align-items-center">
                <div class="d-flex mb-5 align-items-center">
                    <div class="mr-5">
                        <h3 style="font-size: 10rem;" class="nature-spirit-regular text-center primary-color">PRIZE</h3>
                        <img class="mx-auto d-block" src="{{ asset('images/prize.png') }}" alt="">
                    </div>

                    <div>
                        <p class="quicksand-bold">
                            Ten entries will be declared as winners and cash prizes will be used to further support
                            each entries’ implementation of their upcycling solution.
                            <br />
                            <br />
                            
                            •	Grand Prize
                            <br />
                            •	2nd Prize
                            <br />
                            •	3rd Prize
                            <br />
                            •	7 Consolation Prizes
                            <br />
                            <br />

                            To know more about the prizes, request for a pdf download
                            (link with an automatic download of the pdf file)                                 
                        </p>
                    </div>
                </div>

                <div class="d-flex mb-5 align-items-center">
                    <div class="mr-5">
                        <h3 style="font-size: 10rem;" class="nature-spirit-regular text-center primary-color">JUDGES</h3>
                        <img class="mx-auto d-block" src="{{ asset('images/judges.png') }}" alt="">
                    </div>

                    <div>
                        <p class="quicksand-bold">
                            Ten entries will be declared as winners and cash prizes will be used to further support
                            each entries’ implementation of their upcycling solution.
                            <br />
                            <br />

                            •	Grand Prize
                            <br />
                            •	2nd Prize
                            <br />
                            •	3rd Prize
                            <br />
                            •	7 Consolation Prizes
                            <br />
                            <br />

                            To know more about the prizes, request for a pdf download
                            (link with an automatic download of the pdf file)                                 
                        </p>
                    </div>
                </div>

                <div class="d-flex mb-5 align-items-center">
                    <div class="mr-5">
                        <h3 style="font-size: 10rem;" class="nature-spirit-regular text-center primary-color">PARTNERS</h3>
                        <img class="mx-auto d-block" src="{{ asset('images/partners.png') }}" alt="">
                    </div>

                    <div>
                        <p class="quicksand-bold">
                            Ten entries will be declared as winners and cash prizes will be used to further support
                            each entries’ implementation of their upcycling solution.
                            <br />
                            <br />
                            
                            •	Grand Prize
                            <br />
                            •	2nd Prize
                            <br />
                            •	3rd Prize
                            <br />
                            •	7 Consolation Prizes
                            <br />
                            <br />

                            To know more about the prizes, request for a pdf download
                            (link with an automatic download of the pdf file)                                 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection