@extends('layouts.app')

@section('content')
    @include('partials.header', ['title' => 'CALENDAR OF EVENTS'])
    
    @include('partials.subheader', ['subtitle' => 'CALENDAR OF EVENTS'])

    <div class="container inner calendar">
		<div class="row">
			<div class="col-sm-12 col-md-4 mb-5">
				<div id="coe1" class="calendar-body mb-3">
					 <img class="img-fluid" src="images/CTS-MOA.jpg">
				</div>
				<h3 class="quicksand-bold">October 5 - Contract Signing with College Technological School</h3>
			</div>
			<div class="col-sm-12 col-md-4 mb-5">
				<div id="coe2" 	class="calendar-body mb-3">
					<img class="img-fluid" src="images/LAUNCH.jpg">
				</div>
				<h3 class="quicksand-bold"> October 27 - Official Nature's Spring Foundation Launch at Bai Hotel</h3>
			</div>
			
			<div class="col-sm-12 col-md-4 mb-5">
				<div id="coe5" class="calendar-body mb-3">
					<img class="img-fluid" src="images/PBSP.jpg">
				</div>
				<h3 class="quicksand-bold">October 27 - Contract Signing with Oriental and Motolite Marketing Corp (OMMC) and Philippine Business for Social Progres (PBSP) on Balik Baterya Program</h3>
			</div>	

			<div class="col-sm-12 col-md-4 mb-5">
				<div id="coe3" class="calendar-body mb-3">
					<img class="img-fluid" src="images/DIABETIC-1.jpg">

				</div>
				<h3 class="quicksand-bold">November 6-10 - Celebrating World Diabetes Month</h3>
			</div>
			<div class="col-sm-12 col-md-4 mb-5">
				<div id="coe4" class="calendar-body mb-3">
					<img class="img-fluid" src="images/DIABETIC.jpg">

				</div>
				<h3 class="quicksand-bold">Free diabetes check-up in Cebu plant for all employees</h3>
			</div>
			
			<div class="col-sm-12 col-md-4 mb-5">
				<div id="coe6" class="calendar-body mb-3">
					<img class="img-fluid" src="images/TREEPLANTING-1.jpg">

				</div>
				<h3 class="quicksand-bold">December 9 - Tree Planting Activity with PSWRI Staff at Sapangdako, Guadalupe</h3>
			</div>
		</div>
	</div>
@endsection