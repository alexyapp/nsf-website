@extends('layouts.admin')

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script>
        $('#contestants-datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('contestants.datatable') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                { data: 'votes', name: 'votes' },
                { data: 'created_at', name: 'created_at' },
                { data: 'updated_at', name: 'updated_at' },
                { data: 'action', name: 'action', orderable: false, searchable: false }
            ]
        });

        $(document).on('click', '.contestant-delete-button', function(e) {
            e.preventDefault();

            var $this = $(this);

            var result = confirm(`Are you sure you want to delete the contestant #${$this.data('contestantId')}? This action is irreversible.`);

            if (result) {
                $this.parent().find('.contestant-delete-form').submit();
            }
        });
    </script>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row mb-5">
            <div class="col-md-3 justify-content-end">
                <a href="{{ route('contestants.create') }}" class="btn btn-primary">
                    <i class="fa fa-plus"></i> Create Contestant
                </a>
            </div>
        </div>

        @if (session('status'))
            <div class="row mb-3">
                <div class="col-12">
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-12">
                <table id="contestants-datatable" class="datatable">
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Votes</th>
                        <th>Created At</th>
                        <th>Updated At</th>
                        <th>Action</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
@endsection