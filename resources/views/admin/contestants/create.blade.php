@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if (session('resource_exists'))
                    <div class="alert alert-danger">
                        {{ session('resource_exists') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @include('partials.admin.breadcrumb', [
                    'list_items' => [
                        [
                            'label' => 'Contestants',
                            'link' => route('contestants.index')
                        ],
                        [
                            'label' => 'Create',
                            'is_current' => true,
                        ]
                    ]
                ])
                
                <form action="{{ route('contestants.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <div class="d-none">
                            <img class="image-preview img-fluid" src="" alt="Image Preview">
                        </div>

                        <div class="form-group">
                            <label for="profile_picture">Entry Cover</label>
                            <input id="profile_picture" type="file" name="profile_picture" class="previewer form-control-file">

                            {{-- @error('profile_picture')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror --}}
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label for="name">Entry Name</label>
                        <input id="name" type="text" name="name" class="form-control" value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        <label for="description">Entry Bio</label>
                        <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ old('description') }}</textarea>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection