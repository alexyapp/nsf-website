@extends('layouts.admin')   

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            

                <form action="{{ route('settings.store') }}" method="POST">
                    @csrf

                    <div class="mb-3">
                        <div class="d-flex align-items-top">
                            <span class="mr-2">Voting System</span>
                            <div class="d-flex flex-column">
                                <label class="switch mb-0">
                                    <input type="checkbox" name="voting_system" {{ isset($settings['voting_system']) && $settings['voting_system'] == 'on' ? 'checked' : '' }}>
                                    <span class="switch-slider round"></span>
                                </label>
                                <small class="form-text text-muted">Note: turning this feature off will disable the voting system preventing users from casting their votes.</small>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-primary" type="submit">Save</button>
                </form>
            </div>
        </div>
    </div>
@endsection