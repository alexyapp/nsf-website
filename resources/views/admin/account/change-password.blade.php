@extends('layouts.admin')

@push('scripts')
    <script>
        $(document).ready(function() {
            $.each($('.change-password-field'), function() {
                var $this = $(this);

                if ($this.val()) {
                    $this.parent().find('.show-password-button').removeClass('d-none').addClass('d-block');
                }
            });

            $('.change-password-field').on('input', function() {
                var $this = $(this),
                    showPasswordButtonContainer = $this.parent().find('.show-password-button');

                if ($this.val() && showPasswordButtonContainer.hasClass('d-none')) {
                    showPasswordButtonContainer.removeClass('d-none').addClass('d-block');
                } else if (!$this.val()) {
                    showPasswordButtonContainer.addClass('d-none').removeClass('d-block');
                }
            });

            $('.show-password-button').click(function(e) {
                e.preventDefault();

                var $this = $(this),
                    passwordField = $this.parent().find('.change-password-field');

                if (passwordField.attr('type') == 'text') {
                    passwordField.attr('type', 'password');
                    $this.text('Show Password');
                } else if (passwordField.attr('type') == 'password') {
                    passwordField.attr('type', 'text');
                    $this.text('Hide Password');
                }
            });
        });
    </script>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                @if (session('incorrect_old_password'))
                    <div class="alert alert-danger">
                        {{ session('incorrect_old_password') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @include('partials.admin.breadcrumb', [
                    'list_items' => [
                        [
                            'label' => 'Home',
                            'link' => route('admin.dashboard')
                        ],
                        [
                            'label' => 'Change Password',
                            'is_current' => true,
                        ]
                    ]
                ])
                
                <form action="{{ route('account.changePassword') }}" method="POST">
                    @csrf

                    @method('PATCH')

                    <div class="form-group">
                        <label for="old_password">Old Password</label>
                        <input id="old_password" type="password" name="old_password" class="form-control change-password-field 1" value="{{ old('old_password') }}">
                        <a class="d-none mt-1 show-password-button" tabindex="-1" href="#">
                            Show Password
                        </a>
                    </div>

                    <div class="form-group position-relative">
                        <label for="new_password">New Password</label>
                        <input id="new_password" type="password" name="new_password" class="form-control change-password-field 2" value="{{ old('new_password') }}">
                        <a class="d-none mt-1 show-password-button" tabindex="-1" href="#">
                            Show Password
                        </a>
                    </div>

                    <div class="form-group">
                        <label for="new_password_confirmation">New Password (again)</label>
                        <input id="new_password_confirmation" type="password" name="new_password_confirmation" class="form-control change-password-field 3" value="{{ old('new_password_confirmation') }}">
                        <a class="d-none mt-1 show-password-button" tabindex="-1" href="#">
                            Show Password
                        </a>
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection