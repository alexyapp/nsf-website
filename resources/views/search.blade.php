@extends('layouts.app')

@section('content')
    @include('partials.header', ['title' => 'SEARCH'])
        
    @include('partials.subheader', ['subtitle' => 'RESULTS'])

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                @foreach ($results as $result)
                    <div class="mb-5">
                        <a href="{{ route($result->getRouteName()) . '#' . $result->id }}">
                            @if (get_class($result) == 'App\Faq')
                                <p class="quicksand-bold">{{ $result->question }}</p>
                            @endif

                            @if (get_class($result) == 'App\Faq')
                                <div class="quicksand-bold">{!! str_limit(strip_tags($result->answer), 150) !!}</div>
                            @else
                                <div class="quicksand-bold">{!! str_limit(strip_tags($result->content), 150) !!}</div>
                            @endif
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection