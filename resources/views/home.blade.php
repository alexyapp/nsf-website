@extends('layouts.app')

@section('content')
<div class="container-fluid atf px-0 d-flex justify-content-center w-100 position-relative">
    <div class="row justify-content-center">
        <div class="col-12 d-flex flex-column justify-content-center" style="z-index: 1000;">
            <div class="mb-5">
                <img id="the-bote-challenge-logo" class="img-fluid d-block mx-auto" src="{{ asset('images/the-bote-challenge-logo.png') }}" alt="The Bote Challenge Logo">
            </div>

            <div class="mb-5">
                <p style="font-size: .9rem;" class="quicksand-bold text-center mb-0 text-white">
                    The Bote Challenge seeks to utilize the creative energies of children, youth, 
                </p>
                <p style="font-size: .9rem;" class="quicksand-bold text-center mb-0 text-white">
                    families and their local communities in developing lasting solutions for responsible waste management and    
                </p>
                <p style="font-size: .9rem;" class="quicksand-bold text-center mb-0 text-white">
                    promote a healthier, more liveable, and sustainable future for our people and our world.
                </p>
            </div>

            <a href="{{ route('about-the-bote-challenge') }}" class="custom-link quicksand-bold text-white text-center text-underline">
                <u>LEARN MORE</u>
            </a>
        </div>
    </div>
</div>

<div class="container-fluid about text-center">
    <div class="row justify-content-center my-lg-5 my-3">
        <div class="col-lg-8">
            <p class="font-size-xs primary-color quicksand-bold" style="opacity: .73;">The Bote Challenge is an annual search for child-led, innovative solutions within communities to recognize, promote and adopt sustainable practices on proper plastic waste management.</p>
        </div>
    </div>

    <div class="row justify-content-center project-phase-container">
        <div class="col-12 my-5">
            <h3 class="font-size-md primary-color nature-spirit-regular">PROJECT PHASE</h3>
        </div>
        <div class="col-12 project-phase-steps">
            <div class="d-flex align-items-top justify-content-center project-phase-steps-wrapper">

                <div class="step step-one mx-3">
                    <div class="shadow rounded-circle mx-auto mb-5 d-flex align-items-center justify-content-center" style="background-color: #0E7DD3;">
                        <h3 class="text-white nature-spirit-regular">1</h3>
                    </div>
                    <h4 class="quicksand-bold mb-5 primary-color" style="font-size: 2rem;">KICK OFF</h4>
                    <p class="quicksand-bold" style="font-size: 1.12rem;">The Bote Challenge is launched. Nature’s Spring Foundation echoes the campaign to schools, stakeholders and in media platforms.</p>
                </div>
                <div class="step step-two mx-3">
                    <div class="shadow rounded-circle mx-auto mb-5 d-flex align-items-center justify-content-center" style="background-color: #0E7DD3 !important;">
                        <h3 class="text-white nature-spirit-regular">2</h3>
                    </div>
                    <h4 class="quicksand-bold mb-5 primary-color" style="font-size: 2rem;">SUBMIT</h4>
                    <p class="quicksand-bold" style="font-size: 1.12rem;">The <i>school-unities</i> send their entries for the Bote Challenge. Mentors will guide the <i>school-unities</i> in developing and/or enhancing their innovation.</p>
                </div>
                <div class="step step-three mx-3">
                    <div class="shadow rounded-circle mx-auto mb-5 d-flex align-items-center justify-content-center" style="background-color: #0E7DD3 !important;">
                        <h3 class="text-white nature-spirit-regular">3</h3>
                    </div>
                    <h4 class="quicksand-bold mb-5 primary-color" style="font-size: 2rem;">SELECT & VOTE</h4>
                    <p class="quicksand-bold" style="font-size: 1.12rem;">Entries undergo the screening and selection phase together with the identified expert judges. Nature’s Spring Foundation narrows down the selection to 20 finalists which will be open to the public for reading, analyzing and voting.</p>
                </div>
                <div class="step step-four mx-3">
                    <div class="shadow rounded-circle mx-auto mb-5 d-flex align-items-center justify-content-center" style="background-color: #0E7DD3 !important;">
                        <h3 class="text-white nature-spirit-regular">4</h3>
                    </div>
                    <h4 class="quicksand-bold mb-5 primary-color" style="font-size: 2rem;">PROMOTE</h4>
                    <p class="quicksand-bold" style="font-size: 1.12rem;">The Bote Challenge announces the winners. Ten <i>School-unity</i> winners will be featured in platforms where the public can recognize  and learn from the sustainable practices in every winner.</p>
                </div>
                <div class="step step-five mx-3">
                    <div class="shadow rounded-circle mx-auto mb-5 d-flex align-items-center justify-content-center" style="background-color: #0E7DD3 !important;">
                        <h3 class="text-white nature-spirit-regular">5</h3>
                    </div>
                    <h4 class="quicksand-bold mb-5 primary-color" style="font-size: 2rem;">ADOPT</h4>
                    <p class="quicksand-bold" style="font-size: 1.12rem;">Nature’s Spring Foundation will sustain the innovation of the winners through advance assistance programs and possible replication to other communities.</p>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="container-fluid partners">
    <div class="row justify-content-center text-center">
        <div class="col-12">
            <h3 class="font-size-md primary-color nature-spirit-regular text-center">PARTNERS</h3>
        </div>
        <div class="col-md-4">
            <div class="mx-3">
                <div>
                    <img class="img-fluid mb-3" src="{{ asset('images/logo_DepEd.png') }}" alt="DepEd Logo">
                </div>
                <p class="quicksand-bold">DEPARTMENT OF EDUCATION – REGION 7</p>
            </div>  
        </div>
        <div class="col-md-4">
            <div class="mx-3">
                <div>
                    <img class="img-fluid mb-3" src="{{ asset('images/logo_PSWRI.png') }}" alt="PSWRI Logo">
                </div>
                <p class="mb-0 quicksand-bold">PHILIPPINE SPRING WATER RESOURCES, INC.</p>
                <p class="quicksand-bold">(The makers of Nature’s Spring bottled water)</p>
            </div>
        </div>
    </div>
</div>
@endsection
