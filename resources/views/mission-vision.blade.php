@extends('layouts.app')

@section('content')
    @include('partials.header', ['title' => 'MISSION & VISION'])
    
    @include('partials.subheader', ['subtitle' => 'MISSION & VISION'])

    <div class="container">
		<div class="row vertical-center" style="padding: 100px 0;">
			<div class="col-md-6">
				<img class="img-fluid" src="images/mission-img.jpg">
			</div>
			<div class="col-md-5 col-md-offset-1">
				<h2 class="quicksand-bold" style="font-size: 40px; text-transform: capitalize;">Mission</h2>
				<p class="quicksand-bold" style="font-style: italic; font-size: 17px;">To be a partner in improving the lives of every Filipino by delivering programs in the areas of education, health, and sustainable livelihoods.</p>
				<p class="quicksand-bold" style="font-size: 17px;">Its deliberate approach towards partnerships in the improvement of the lives of every Filipino means the foundation will not provide dole-outs of support and assistance. Partnering implies taking a journey together towards creating lasting and impactful solutions for the communities it will serve, delivering quality programs that respond to needs while building on strengths and assets of its people.</p>
			</div>
		</div>
		<div class="row vertical-center" style="padding-bottom: 100px;">
			<div class="col-md-5">
				<h2 class="quicksand-bold" style="font-size: 40px; text-transform: capitalize;">Vision</h2>
				<p class="quicksand-bold" style="font-style: italic; font-size: 17px;">A Philippines with empowered people committed to safe, sustainable, and healthy communities.</p>
				<p class="quicksand-bold" style="font-size: 17px;">The foundation sees a country that is vibrant and thriving because its people are empowered to take action towards safe, sustainable and healthy communities.</p>
			</div>
			<div class="col-md-6 col-md-offset-1">
				<img class="img-fluid" src="images/vision-bg.jpg">
			</div>
		</div>
	</div>
@endsection