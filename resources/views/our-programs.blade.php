@extends('layouts.app')

@section('content')
    @include('partials.header', ['title' => 'OUR PROGRAMS'])
    
    @include('partials.subheader', ['subtitle' => 'OUR PROGRAMS'])

    <div class="container">
		<div class="row vertical-center" style="padding: 100px 0;">
			<div class="col-md-6">
				<img class="img-fluid" src="images/mission-img.jpg">
			</div>
			<div class="col-md-5 col-md-offset-1">
				<h2 class="quicksand-bold" style="font-size: 40px; text-transform: capitalize;">Mission</h2>
				<p class="quicksand-bold" style="font-size: 17px;">To realize the mission, the foundation has identified 3 focus areas or thematic sectors and set measurable goals against them for the next 3-years. These goals are what will become the programmatic goals that will aim to meet customer needs for the foundation.</p>
			</div>
		</div>
		<div class="row vertical-center" style="padding-bottom: 100px;">
			<div class="col-md-6">

				<h3 class="quicksand-bold" style="font-size: 25px; text-transform: capitalize;">Education Goal</h3>
				<p class="quicksand-bold" style="font-size: 17px;">Developed education programs that would increase employability, life skills, and entrepreneurship.</p>
				
				<h3 class="quicksand-bold" style="font-size: 25px; text-transform: capitalize;">To achieve :</h3>
				<ol class="quicksand-bold" style="font-size: 17px;">
					<li>Provide educational scholarships and training opportunities to employees' children and out of school youth</li>
					<li>
						Implement an "Adopt A School" program in Mandaue city to include:
						<ul>
							<li>
								Teacher's training, teaching aids and facilities
							</li>
							<li>
								Basic school infrastructure (classroom, laboratory, library)
							</li>
						</ul>
					</li>
				</ol>

				<h3 class="quicksand-bold" style="font-size: 25px; text-transform: capitalize;">Health Goal</h3>
				<p class="quicksand-bold" style="font-size: 17px;">Improved fitness and well being of employees and target communities.</p>
				<h3 class="quicksand-bold" style="font-size: 25px; text-transform: capitalize;">To achieve :</h3>
				<ol class="quicksand-bold" style="font-size: 17px;">
					<li>Conduct diabetes awareness and testing for company employees nationwide and the city of Mandaue; and,</li>
					<li>Partner with the PSWRI in sponsoring activities that promote overall physical fitness and health.</li>
				</ol>
			</div>

			<div class="col-md-5 col-md-offset-1">
				<h3 class="quicksand-bold" style="font-size: 25px; text-transform: capitalize;">Environmental Goal</h3>
				<p class="quicksand-bold" style="font-size: 17px;">Promoted environmental awareness, stewardship, and sustainability.</p>
				<ol class="quicksand-bold" style="font-size: 17px;">
					<li>'Adopt a watershed' program in Kotkot, Mananga, and Lusaran watersheds in Cebu; (reforestation, urban greening);</li>
					<li>Develop and implement waste reduction livelihood project (with DSWD, DOLE);</li>
					<li>"Empty Bottles for School Supplies" Swap (linked w/ Adopt A School Program)</li>
				</ol>
			</div>
		</div>
	</div>
@endsection