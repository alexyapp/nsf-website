@extends('layouts.app')

@section('content')
    @include('partials.header', ['title' => 'ABOUT US'])
    
    @include('partials.subheader', ['subtitle' => 'ABOUT US'])

    <div class="container inner about-us" style="padding: 100px 0;">
		<div class="row">
			<div class="col-md-6">
				<img class="img-fluid" src="images/about.jpeg">
			</div>
			<div class="col-md-6">
				<h2 class="quicksand-bold" style="font-size: 40px; text-transform: capitalize; color: #232323;">Nature Spring Foundation</h2>
				<p class="quicksand-bold" style="font-size: 16px;">Danilo Lua and family have been personally engaged in numerous philanthropic activities in the past and it was only a matter of time for them to formally set-up a family foundation as a natural extension of their compassion for others and the community. Philippine Spring Water Resources Inc. launches its corporate social responsibility (CSR) program through the Lay Kim and Corazon Foundation, Inc now known as The Nature's Spring Foundation, Inc. as a response to its obligation to
consider the interests of its customers, employees, and the communities it
influences and to consider the social and environmental impacts of its economic and business activities.</p>
			</div>
		</div>
	</div>
@endsection