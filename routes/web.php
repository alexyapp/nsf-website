<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@home')->name('home');

Route::get('/about-the-bote-challenge', 'PageController@about')->name('about-the-bote-challenge');

Route::group(['prefix' => 'how-to-join'], function () {
    Route::get('/mechanics', 'PageController@mechanics')->name('mechanics');
    Route::get('/guidelines', 'PageController@guidelines')->name('guidelines');
    Route::get('/criteria', 'PageController@criteria')->name('criterias');
});

Route::get('faqs', 'PageController@faqs')->name('faqs');

Route::view('about-us', 'about-us')->name('about-us');
Route::view('mission-vision', 'mission-vision')->name('mission-vision');
Route::view('our-programs', 'our-programs')->name('our-programs');
Route::view('calendar-of-events', 'calendar-of-events')->name('calendar-of-events');

Route::get('finalists', 'PageController@finalists')->name('finalists.index');

Route::get('search', 'SearchController')->name('search');

Route::group(['namespace' => 'Auth'], function () {
    Route::post('logout', 'LoginController@logout')->name('logout');

    Route::group(['prefix' => 'admin'], function () {
        Route::get('login', 'LoginController@showLoginForm')->name('login');
        Route::post('login', 'LoginController@login');
    });
});

Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('dashboard', 'AdminController@index')->name('admin.dashboard');

        Route::group(['prefix' => 'contestants'], function () {
            Route::get('/', 'ContestantController@index')->name('contestants.index');
            Route::get('create', 'ContestantController@create')->name('contestants.create');
            Route::get('datatable', 'ContestantController@datatable')->name('contestants.datatable');
            Route::get('/{contestant}/edit', 'ContestantController@edit')->name('contestants.edit');
            Route::post('/', 'ContestantController@store')->name('contestants.store');
            Route::patch('/{contestant}', 'ContestantController@update')->name('contestants.update');
            Route::delete('/{contestant}', 'ContestantController@destroy')->name('contestants.destroy');
        });

        Route::group(['prefix' => 'account'], function () {
            Route::get('change-password', 'AccountController@showChangePasswordForm')->name('account.showChangePasswordForm');
            Route::patch('change-password', 'AccountController@changePassword')->name('account.changePassword');
        });

        Route::group(['prefix' => 'settings'], function () {
            Route::get('/', 'SettingController@index')->name('settings.index');
            Route::post('/', 'SettingController@store')->name('settings.store');
        });
    });
});

Route::group(['prefix' => 'finalists'], function () {
    Route::get('/vote', 'ContestantController@vote')->name('finalists.vote')->middleware('facebook', 'signed');
    Route::get('/{finalist}', 'ContestantController@show')->name('contestants.show');
});

Route::get('/auth/redirect/{provider}', 'SocialiteController@redirect')->name('facebook.login');
Route::get('/callback/{provider}', 'SocialiteController@callback')->name('facebook.callback');

Route::get('/privacy-policy', 'PageController@privacyPolicy')->name('privacy-policy');
Route::get('/terms-and-conditions', 'PageController@termsAndConditions')->name('terms-and-conditions');

Route::get('/home', 'HomeController@index')->name('home');
