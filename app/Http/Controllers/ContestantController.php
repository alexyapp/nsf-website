<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Contestant;
use App\Http\Requests\CreateContestantRequest;
use Image;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class ContestantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.contestants.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contestants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CreateContestantRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateContestantRequest $request)
    {
        if ($request->hasFile('profile_picture')) {
            $raw = $request->file('profile_picture');
            
            $processed = Image::make($raw);

            $filename = time() . '.' . $raw->extension();

            $path = public_path().'/uploads/';

            $processed->resize(1200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $processed->save($path . $filename);

            $path = public_path().'/thumbnails/';

            $processed->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $processed->save($path . $filename);
        }

        try {
            Contestant::create(
                array_merge(
                    $request->only('name', 'description'),
                    [
                        'profile_picture' => $request->hasFile('profile_picture') ? $filename : null
                    ]
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];

            // unique constraint
            if ($errorCode == 1062) {
                return redirect()->back()->withInput()->with('resource_exists', 'Entry Name already exists.');
            }
        }

        return redirect()->back()->with('status', 'Contestant created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contestant  $contestant
     * @return \Illuminate\Http\Response
     */
    public function show(Contestant $finalist)
    {
        $voting_system_setting = Setting::withKey('voting_system')->first();

        return view('finalists.show', compact('finalist', 'voting_system_setting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contestant  $contestant
     * @return \Illuminate\Http\Response
     */
    public function edit(Contestant $contestant)
    {
        return view('admin.contestants.edit', compact(['contestant']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contestant  $contestant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contestant $contestant)
    {
        if ($request->hasFile('profile_picture')) {
            $raw = $request->file('profile_picture');
            
            $processed = Image::make($raw);

            $filename = time() . '.' . $raw->extension();

            $path = public_path().'/uploads/';

            $processed->resize(1200, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $processed->save($path . $filename);

            $path = public_path().'/thumbnails/';

            $processed->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $processed->save($path . $filename);
        }

        try {
            $contestant->update(
                array_merge(
                    $request->only('name', 'description'),
                    [
                        'profile_picture' => $request->hasFile('profile_picture') ? $filename : null
                    ]
                )
            );
        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];

            // unique constraint
            if ($errorCode == 1062) {
                return redirect()->back()->withInput()->with('resource_exists', 'Entry Name already exists.');
            }
        }

        return redirect()->back()->with('status', 'Contestant updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contestant  $contestant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contestant $contestant)
    {
        $contestant->delete();

        return redirect()->back()->with('status', 'Contestant deleted successfully!');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function datatable()
    {
        return Datatables::of(Contestant::all())
            ->editColumn('description', function ($contestant) {
                return str_limit($contestant->description, 30);
            })
            ->editColumn('created_at', function ($contestant) {
                return Carbon::parse($contestant->created_at)->format('F d, Y h:i A');
            })
            ->editColumn('updated_at', function ($contestant) {
                return Carbon::parse($contestant->updated_at)->format('F d, Y h:i A');
            })
            ->addColumn('action', function ($contestant) {
                $editUrl = route('contestants.edit', $contestant->id);
                $deleteUrl = route('contestants.destroy', $contestant->id);
                $csrf = csrf_token();

                return "<div class='d-flex align-items-center'><a class='btn btn-primary' href='{$editUrl}'>Edit</a> <a class='btn btn-danger ml-1 contestant-delete-button' href='#' data-contestant-id='{$contestant->id}'>Delete <form class='d-none contestant-delete-form' method='POST' action='{$deleteUrl}'><input type='hidden' name='_token' value='{$csrf}'> <input type='hidden' name='_method' value='delete' /></form></a></div>";
            })
            ->addColumn('votes', function ($contestant) {
                return $contestant->votes->count();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * 
     */
    public function vote()
    {
        $voting_system_setting = Setting::withKey('voting_system')->first();

        if (optional($voting_system_setting)->value == 'on') {
            try {
                $finalist = Contestant::find(request('finalist_id'));
    
                $finalist->votes()->create([
                    'user_id' => auth()->id()
                ]);
    
                auth()->logout();
    
                Session::flush();
            } catch (\Illuminate\Database\QueryException $e) {
                auth()->logout();
    
                Session::flush();
    
                $errorCode = $e->errorInfo[1];
    
                // unique constraint
                if ($errorCode == 1062) {
                    return redirect()->route('contestants.show', $finalist->id)->with('has_voted', 'Please be advised that we only allow one vote per Facebook account. Thank you!');
                }
            }
    
            return redirect()->route('contestants.show', $finalist->id)->with('success', 'Thank you for voting. Thirty percent (30%) will be taken from the votes.');
        }
    }
}
