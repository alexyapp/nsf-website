<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Show the change password form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showChangePasswordForm()
    {
        return view('admin.account.change-password');
    }

    /**
     * Change account password.
     * 
     * @param \App\Http\Requests\ChangePasswordRequest $request
     * @return \Illuminate\Http\Response
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();

        if (Hash::check(request('old_password'), $user->password)) {
            $user->update([
                'password' => Hash::make(request('new_password'))
            ]);
    
            return redirect()->back()->with('status', 'Password updated successfully!');
        }

        return redirect()->back()->withInput()->with('incorrect_old_password', 'Incorrect old password.');
    }
}
