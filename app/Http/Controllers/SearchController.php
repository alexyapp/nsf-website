<?php

namespace App\Http\Controllers;

use App\Guideline;
use App\Criteria;
use App\Faq;
use App\Mechanic;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    /**
     * 
     */
    public function __invoke()
    {
        if ($q = request()->q) {
            $mechanics = Mechanic::search($q)->get();
            $criterias = Criteria::search($q)->get();
            $faqs = Faq::search($q)->get();
            $guidelines = Guideline::search($q)->get();

            $results = $mechanics->merge($criterias)->merge($faqs)->merge($guidelines);

            return view('search', compact('results'));
        }

        return redirect()->route('home');
    }
}
