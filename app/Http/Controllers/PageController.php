<?php

namespace App\Http\Controllers;

use App\Guideline;
use App\Faq;
use App\Mechanic;
use App\Criteria;
use App\Contestant;
use Illuminate\Support\Collection;

class PageController extends Controller
{
    /**
     * Show the home page.
     */
    public function home()
    {
        return view('home');
    }

    /**
     * Show the about the bote challenge page.
     */
    public function about()
    {
        return view('about');
    }

    /**
     * Show the mechanics page.
     */
    public function mechanics()
    {
        $mechanics = Mechanic::orderBy('sequence_number', 'asc')->get();

        return view('how-to-join.mechanics', compact('mechanics'));
    }

    /**
     * Show the guidelines page.
     */
    public function guidelines()
    {
        $guidelines = Guideline::orderBy('sequence_number', 'asc')->get();

        return view('how-to-join.guidelines', compact('guidelines'));
    }

    /**
     * Show the criteria page.
     */
    public function criteria()
    {
        $criterias = Criteria::orderBy('sequence_number', 'asc')->get();

        return view('how-to-join.criteria', compact('criterias'));
    }

    /**
     * Show the faqs page.
     */
    public function faqs()
    {
        $faqs = Faq::orderBy('sequence_number', 'asc')->get();

        return view('faqs.index', compact('faqs'));
    }

    /**
     * Show the finalists page.
     */
    public function finalists()
    {
        $finalists = Contestant::all();
        $finalists_even = new Collection();
        $finalists_odd = new Collection();

        $finalists->each(function ($finalist, $i) use ($finalists_even, $finalists_odd) {
            if ($i % 2 == 0) {
                $finalists_even->push($finalist);
            } else {
                $finalists_odd->push($finalist);
            }
        });

        return view('finalists.index', compact(['finalists', 'finalists_even', 'finalists_odd']));
    }

    /**
     * Show the privacy policy page.
     */
    public function privacyPolicy()
    {
        return view('privacy-policy.index');
    }

    /**
     * Show the terms and conditions page.
     */
    public function termsAndConditions()
    {
        return view('terms-and-conditions.index');
    }
}
