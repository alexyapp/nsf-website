<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        $settings = [];

        Setting::all()->each(function ($setting) use (&$settings) {
            $settings[$setting->key] = $setting->value;
        });

        return view('admin.settings.index', compact('settings'));
    }

    public function store()
    {
        if (!request()->has('voting_system')) {
            $data = request()->except('_token');
            $data['voting_system'] = 'off';
            request()->replace($data);
            $data = request()->all();
        } else {
            $data = request()->except('_token');
        }

        foreach ($data as $key => $value) {
            Setting::updateOrCreate([
                'key' => $key
            ], [
                'key' => $key,
                'value' => $value
            ]);
        }

        return redirect()->back()->with('status', 'Settings updated successfully!');
    }
}
