<?php

namespace App\Http\Controllers;

use App\User;
use Socialite;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

class SocialiteController extends Controller
{
    public function redirect($provider)
    {
        return Socialite::driver($provider)->with(['state' => request('finalist_id')])->redirect();
    }

    public function callback($provider)
    {
        $getInfo = Socialite::driver($provider)->stateless()->user(); 
        $user = $this->createUser($getInfo, $provider);

        auth()->login($user);
        
        if (request()->has('state')) {
            $finalist_id = request('state');

            return redirect(URL::signedRoute('finalists.vote', ['finalist_id' => $finalist_id]));
        }
    }

    public function createUser($getInfo, $provider)
    {
        $user = User::where('provider_id', $getInfo->id)->first();

        if (!$user) {
            $user = User::create([
                'name'     => $getInfo->name,
                'email'    => $getInfo->email,
                'provider' => $provider,
                'provider_id' => $getInfo->id
            ]);
        }

        return $user;
    }
}
