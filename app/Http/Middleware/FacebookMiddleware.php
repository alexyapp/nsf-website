<?php

namespace App\Http\Middleware;

use Closure;

class FacebookMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            return $next($request);
        }

        return redirect()->route('facebook.login', ['driver' => 'facebook', 'finalist_id' => $request->finalist_id]);
    }
}
