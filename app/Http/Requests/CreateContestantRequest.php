<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateContestantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'profile_picture' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'name' => 'required|string',
            'description' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'profile_picture.required' => 'Entry Cover Photo field is required.',
            'profile_picture.image' => 'Entry Cover Photo must be an image.',
            'profile_picture.mimes' => 'Entry Cover Photo must be a file of type: jpeg, png, jpg.',
            'name.required' => 'Entry Name field is required.',
            'description.required' => 'Entry Bio field is required.'
        ];
    }
}
