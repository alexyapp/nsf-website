<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseSearchableModel extends Model
{
    public function getRouteName()
    {
        $className = get_class($this);

        return str_plural(strtolower(explode('\\', $className)[1]));
    }
}
