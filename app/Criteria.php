<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class Criteria extends BaseSearchableModel
{
    use Searchable;

    protected $fillable = [
        'sequence_number', 'content'
    ];
}
