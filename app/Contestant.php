<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contestant extends Model
{
    protected $fillable = [
        'name', 'description', 'profile_picture'
    ];

    public function votes()
    {
        return $this->hasMany(Vote::class);
    }
}
