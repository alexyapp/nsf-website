<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
        'key', 'value'
    ];

    public function scopeWithKey($query, $key)
    {
        return $query->where('key', $key);
    }
}
